<?php
session_start();

// Appel de la page qui gère les accès aux pages
require_once 'src/listePages.php';

// Appel de la page qui gère l'entete, le menu et le pied de page du site
require_once 'src/presentation.php';

// Appel de la page qui gère les affichages de la page accueil
require_once 'src/controller/controller_accueil.php';

// Appel de la page qui gère les affichages de la page accueil
require_once 'src/controller/controller_cadeaux.php';



// Appel de la page qui gère les affichages concernant l'administrateur
require_once 'src/controller/controller_admin.php';
// Appel de la page qui gère les affichages concernant les types
require_once 'src/controller/controller_type.php';
// Appel de la page qui gère les affichages concernant les clients
require_once 'src/controller/controller_invites.php';
// Appel de la page qui gère les affichages concernant le contact
require_once 'src/controller/controller_contact.php';

// Appel la page qui contient l'ensemble des paramètre pour ce connecter à la base de donnée
require_once 'src/config.php';

// Appel de la base de donnée
require_once 'src/bd/connexion.php';

// Appel de la d'une fonction qui génére aléatoirement un mot de passe
require_once 'src/mdp.php';

// Ajout des actions que l'on peut avoir sur la table client
require_once 'classes/classe_cadeau.php';

// Ajout des actions que l'on peut avoir sur la table type
require_once 'classes/classe_type.php';

// Ajout des actions que l'on peut avoir sur la table produit
require_once 'classes/classe_invites.php';




$db = connect($config); // Connexion à la base de données
$contenu = getPage();


// Si la base de données renvoie une valeur différente de NULL
if ($db != NULL){


    
    
    entete();
    // On affiche la page normalement
    menu() ;
    
    // Demande le nom de la fonction qui gère l'affichage de la page souhaitée

    // Exécution de la fonction souhaitée
    $contenu($db);
    
    // On affiche le bas de la page
    bas();
}


// Sinon nous indiquons à l'utilisateur que le site est indisponible
else {

echo'<!DOCTYPE html>
     <html lang="fr">
        <head>
            <meta charset="utf-8">
            <title>Erreur - Guy Mollet Shop</title>
            <link rel="stylesheet" href="css/erreur.css">
        </head>
        <body class="erreur" id="erreur">
            <h1 data-title="Le site est indisponible pour quelques instants">Erreur</h1>
        </body>
     </html>';
}

?> 