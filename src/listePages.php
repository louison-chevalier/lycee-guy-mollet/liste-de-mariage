<?php

function getPage(){
            
    // On déclare dans le tableau $lesPages, une clé et une valeur
    // La clé est ajoutType, la valeur est ajoutType
	
    
    // 0 voudra dire « tout le monde »
    $lesPages['accueil'] = "accueil;0";
    
    $lesPages['start'] = "start;0";
    $lesPages['cadeaux'] = "cadeaux;0";
      $lesPages['contact'] = "contact;0";
    $lesPages['login'] = "login;0";
    // 1 voudra dire « uniquement administrateur »
    $lesPages['accueiladmin'] = "accueiladmin;0";
    
    $lesPages['listetypeadmin'] = "listetypeadmin;0";
    $lesPages['ajouttypeadmin'] = "ajouttypeadmin;0"; 
    $lesPages['inscriptiontypeadmin'] = "inscriptiontypeadmin;0";
    $lesPages['supprimetypeadmin'] = "supprimetypeadmin;0";
    
    $lesPages['categoriescadeaux'] = "categoriescadeaux;0"; 
    $lesPages['listecadeauadmin'] = "listecadeauadmin;0";
    $lesPages['ajoutcadeauadmin'] = "ajoutcadeauadmin;0"; 
    $lesPages['formcadeauadmin'] = "formcadeauadmin;0";
    $lesPages['supprimecadeauadmin'] = "supprimecadeauadmin;0"; 
   
    
    
    $lesPages['supprimeclientadmin'] = "supprimeclientadmin;0";
    $lesPages['modifclientadmin'] = "modifclientadmin;0"; 
        
    $lesPages['listetypeadmin'] = "listetypeadmin;0"; 
    $lesPages['ajouttypeadmin'] = "ajouttypeadmin;0"; 
    $lesPages['inscriptiontypeadmin'] = "inscriptiontypeadmin;0";
    $lesPages['supprimetypeadmin'] = "supprimetypeadmin;0"; 
    $lesPages['modiftypeadmin'] = "modiftypeadmin;0"; 
     $lesPages['modificationtypeadmin'] = "modificationtypeadmin;0";
        
        
    $lesPages['listeinviteadmin'] = "listeinviteadmin;0";
    $lesPages['supprimeinviteadmin'] = "supprimeinviteadmin;0"; 
    $lesPages['forminviteadmin'] = "forminviteadmin;0";
    $lesPages['ajoutinviteadmin'] = "ajoutinviteadmin;0";


    $lesPages['achatcadeaux'] = "achatcadeaux;0"; 
        
    
	// regarde si en mémoire, une variable « page » vient d'un lien ($_GET)
	if(isset($_GET['page'])){
	// Nous mettons dans la variable $page, la valeur qui a été passée dans le lien
	$page = $_GET['page']; }
	else{
	// S'il n'y a rien en mémoire, nous lui donnons la valeur « accueil » afin de lui afficher une page
	//par défaut
	$page = 'accueil';
	}
	// Nous regardons si la page passée en paramètre est une clé du tableau $lesPages
	if (!isset($lesPages[$page])){
	// Nous rentrons ici si cela n'existe pas, ainsi nous redirigeons l'utilisateur sur la page d'accueil
	$page = 'accueil'; }
	// Nous explosons la valeur du tableau correspondant à la clé
        // C'est le ; qui nous sert de repère
	$explose = explode(";",$lesPages[$page]);
        
        
        $rolePage = $explose[1];
        // Si la page est pour tout le monde
        
        if($rolePage == 0){
            // le contenu est directement affecté
            $contenu = $explose[0];
        }
        
        else{
            // Je regarde si la personne est connectée
            if (isset($_SESSION['login'])){
                // Je récupère le rôle de la personne s’il existe
                
                if (isset($_SESSION['role'])){
                    // Je regarde si le rôle de la personne correspond au rôle qu’il faut avoir pour lire
                // la page
                    if($rolePage==$_SESSION['role']){
                    // C’est le cas donc je mets le contenu
                    $contenu = $explose[1];
                    }
                    
                    else{
                        // Il n’a pas les droits donc je mets la page d’accueil
                       // $contenu = 'accueil';
                        $contenu = $explose[1];
                    }
                }
                
                else{
                // La personne n’est pas connectée donc on lui donne la page d’accueil
                $contenu = 'accueil'; }
            }
            else{
                $contenu = 'accueil'; }
            }
        return $contenu;
	}
?>
