<?php
       
 function listeTypes($db){
    echo '<h2>Liste des catégories </h2><br>';
   
    $type = new type($db);
    $listetype = $type->selectAll();

    echo '<table class="table table-striped">
            <THEAD>
                <tr id="titre">
                    <th >id</th>
                    <th>nom</th>
                </tr>
            </THEAD>
            <TBODY>
    ';

    foreach($listetype as $unType){
    
    echo '<tr>
            <td>'.utf8_encode($unType['idtype'] ).'</td>
            <td>'.utf8_encode($unType['nomtype'] ).'</td>
          </tr>';
        }
        
    echo '</TBODY>';
    echo '</table>';

 }
 
 
 function ajoutType($db){
     
    if (isset($_POST['btajoutType'])){
      $nomtype = $_POST['nomtype'];
      //var_dump($_POST);
      // var_dump est une fonction qui affiche la structure et le    
      //contenu d'une variable, cela est pratique pour les tableaux 
      //et pour voir si les valeurs ont bien été passées
      //décommentez la ligne pour que le var_dump s’exécute
      $type = new type($db);  
      $nb = $type->insertAll($nomtype);
      
      if ($nb!=1){
        echo '
            <br>
            <div class="alert alert-danger" role="alert">Attention erreur d\'insertion </div>
            </div>
            <a href="index.php?page=inscriptiontypeadmin">
                <button class="btn btn-default">ESSAYER A NOUVEAU</button>
            </a>
            <br>
            <br>
            <br>
            ';
        }

        else{
        echo'
            <br>
            <div class="alert alert-success" role="alert">Insertion réussie</div>
            </div>
            <a href="index.php?page=inscriptiontypeadmin">
                <button class="btn btn-default">AJOUTER UNE NOUVELLE CATÉGORIE</button>
            </a>
            <br>
            <br>
            <br>
            ';
        } 
    }
}
 

function inscriptionType($db){
    echo'
        <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="login">
                <div class="login-triangle"></div>
                    <h2 class="login-header">Ajout Catégorie</h2>
                    
                    <form class="login-container" method="post" action="index.php?page=ajouttypeadmin" method="post">
                       <p><input placeholder="Nom Type"   type="text"     class="inscription" id="inscription" type="text" id="nomtype" name="nomtype"  required autocomplete="on"></p>
                      
                       <p><input type="submit" id="btajoutType" name="btajoutType" value="Valider"></p>
                    </form>
                    <br>
                    
                </div>
            </div>
        </div
    </div>
    

        
    ';
}


  function supprimeType($db){
    
    if(isset($_POST['btSupprimer'])){
        
        if (isset($_POST['cocher'])){
            $liste = $_POST['cocher'];
            $type = new type($db);
            
            foreach($liste as $idtype){
                
                $nb = $type->deleteOne($idtype);
                
            }
        }
    }

    echo '<h2>Suppression de catégorie</h2><br>';
   
    $type = new type($db);
    $liste = $type->selectAll();
    
    if (count($liste)>0){
        
        echo '<form method="post" action="index.php?page=supprimetypeadmin">';
        echo '<table class="table table-striped">
                <THEAD>
                    <tr>
                        <th>Nom</th>
                        <th class="element">Sélectionner un ou des élèment(s) à supprimer</th>
                    </tr>
                <THEAD>
                <TBODY>
        ';

        foreach($liste as $unType){
            echo '<tr><td> '.utf8_encode($unType['nomtype']).'</td><td><input type="checkbox" name="cocher[]" value="'.$unType['idtype'].'"/></td></tr>';
        }
        echo'</TBODY>';
        echo '</table>';
        echo '<input type="submit" name="btSupprimer" value="Supprimer" />' ;
        echo '</form><br>';
    } 
    
    else{
    echo 'La liste est vide' ;
    
    }
}






function modifType($db){
    echo '<h2>Liste des catégories </h2><br>';
   
    $type = new type($db);
    $listetype = $type->selectAll();

    echo '<table class="table table-striped">
            <THEAD>
                <tr id="titre">
                    <th >id</th>
                    <th>Sélection</th>
                </tr>
            </THEAD>
            <TBODY>
    ';

    foreach($listetype as $unType){
    
    echo '<tr>
            <td><a href="index.php?page=modificationtypeadmin&idtype='.$unType['idtype'].'" >'.utf8_encode($unType['nomtype']).'</a></td>
            <td><input type="checkbox" name="cocher[]" value="'.$unType['nomtype'].'"/></td>
          </tr>';
        }
        
    echo '</TBODY>';
    echo '</table>';

 }


 function modificationType($db){
    if(isset($_POST['btModifier'])){
        $idtype = $_POST['idtype'];
        $nomtype = $_POST['nomtype'];
        $type = new type($db);
        $nb = $type->updateAll($idtype, $nomtype);
    }


    if (isset($_GET['idtype'])){
        $idtype = $_GET['idtype'];
    }

    $type = new type($db);
    $unType = $type->selectOne($idtype);

    echo'<form action="index.php?page=modificationtypeadmin" method="post">
        <input type="hidden" name="idtype"  value="'.$idtype.'" />
        <label for="nom">Nom du type :</label><input type="text" name="nomtype" value="'.$unType['nomtype'].'" />
        <input type="submit" name="btModifier" id="btModifier" value="Modifier">
        </form>';
}


 ?>