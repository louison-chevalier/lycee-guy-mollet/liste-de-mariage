<?php


function menuadmin(){
    echo '   
    <header  role="banner" style="background-image:url(assets/images/img_bg_5.jpg);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn" style=" padding-top:110px;">
                            <h2>ADMINISTRATEUR</h2>
                            <div class="simply-countdown simply-countdown-one"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid">
          ';
    
    echo' 
    <nav class="navbar navbar-default sidebar" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>      
            </div>
            <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                <ul class="nav navbar-nav">
                
                    <li ><a href="index.php?page=accueiladmin">GESTION DU SITE :<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon "></span></a></li>
        
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">CATÉGORIE <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon "></span></a>
                        <ul class="dropdown-menu forAnimate" role="menu">
                            <li><a href="index.php?page=listetypeadmin">Liste</a></li>
                            <li><a href="index.php?page=inscriptiontypeadmin">Ajout</a></li>
                            <li><a href="index.php?page=supprimetypeadmin">Suppression</a></li>
                            <li><a href="index.php?page=modiftypeadmin">Modification</a></li>

                        </ul>
                    </li>
        
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">CADEAUX <span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon"></span></a>
                        <ul class="dropdown-menu forAnimate" role="menu">
                            <li><a href="index.php?page=listecadeauadmin">Liste</a></li>
                            <li><a href="index.php?page=formcadeauadmin">Ajout</a></li>
                            <li><a href="index.php?page=supprimecadeauadmin">Suppression</a></li>
                        </ul>
                    </li>
        
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">INVITÉS<span class="caret"></span><span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon"></span></a>
                        <ul class="dropdown-menu forAnimate" role="menu">
                            <li><a href="index.php?page=listeinviteadmin">Liste</a></li>
                            <li><a href="index.php?page=forminviteadmin">Ajout</a></li>
                            <li><a href="index.php?page=supprimeinviteadmin">Suppression</a></li>
                        </ul>
                    </li>
                    
                </ul>
            </div>
        </div>
    </nav>
    ';
}

function piedadmin(){
   
    echo'</div>';
    after();
}

function accueiladmin(){

menuadmin(); 
echo' 
        <p>Bienvenue dans votre espace administrateur, <br>
Depuis cette interface pour pourrait facilement visualiser, modifier, supprimer et ajouter les différents élèment que comporte ce site. 

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 

Lorem ipsum dolor sit amunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	';

piedadmin();
}


/******************************************************************************/
/********************* GESTION INVITES - ADMINISTRATEUR ***********************/
/******************************************************************************/

function forminviteadmin($db){
    menuadmin(); 
    ajouterClient($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}

function ajoutinviteadmin($db){
    menuadmin(); 
    ajoutClient($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}


function listeinviteadmin($db){
    menuadmin(); 
    listeinvite($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}


function supprimeinviteadmin($db){
    menuadmin(); 
    supprimeInvite($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}


/******************************************************************************/
/********************** GESTION CADEAUX- ADMINISTRATEUR ***********************/
/******************************************************************************/

function listecadeauadmin($db){
    menuadmin(); 
    listecadeau($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';      
     piedadmin();
}

function formcadeauadmin($db){
    menuadmin(); 
    formCadeau($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';
    piedadmin();     
}

function ajoutcadeauadmin($db){
    
    menuadmin(); 
    ajoutCadeau($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';
     piedadmin();     
}

function supprimecadeauadmin($db){
    
    menuadmin(); 
    supprimeCadeau($db);
    
     echo'           </div>
                </div>
            </div>
        </div>
    ';
    piedadmin();        
}






/******************************************************************************/
/******************** GESTION CATÉGORIE- ADMINISTRATEUR ***********************/
/******************************************************************************/



function listetypeadmin($db){
    menuadmin(); 
    listeTypes($db);
    piedadmin();
}

function ajouttypeadmin($db){
    menuadmin(); 
    ajoutType($db);
    piedadmin();
}

function inscriptiontypeadmin($db){
    menuadmin(); 
    inscriptionType($db);
    piedadmin();
}

function supprimetypeadmin($db){
    menuadmin(); 
    supprimeType($db);
    piedadmin();
}

function modiftypeadmin($db){
    menuadmin(); 
    modifType($db);
    piedadmin();
}

function modificationtypeadmin($db){
    menuadmin(); 
    modificationType($db);
    piedadmin();
}


?>
