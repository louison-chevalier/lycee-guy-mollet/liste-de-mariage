<?php

function formProduit($db){
    
echo' 
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="login">
                <div class="login-triangle"></div>
                
                    <h2 class="login-header">Ajout Produit</h2>
                    <form class="login-container" method="post" action= "index.php?page=ajoutproduitadmin" enctype="multipart/form-data">
                       <p><input placeholder="Identifiant"   type="text"     id="identifiant"     name="identifiant"   required autocomplete="on"></p>
                       <p><input placeholder="designation"   type="text"     id="designation"     name="designation"   required autocomplete="on"></p>
                       <p><input placeholder="prix"          type="text"     id="prix"            name="prix"          required autocomplete="on"></p>
                      
                       <p>
                            <select id= "idtype" name="idtype">
                             <optgroup label=" - - - SÉLECTIONNER UN TYPE - - - "> ';
                            $type = new type($db);
                            $listetype = $type->selectAll();
                             foreach($listetype as $unType){
                             echo'<option value="'.$unType['idtype'].'" selected>'.$unType['nomtype'].'</option>';
                             
                             }
                           
                            echo'
                                </optgroup>
                            </select>
                       </p>
                       <p><label for="photo">Photo :</label><input type="file" name="photo" id="photo"/><br /></p>
                       <p><input type="submit" id="btAjouter" name="btValider" value="Valider"></p>
                    </form>
                    <br>
                    
                </div>
            </div>
        </div
    </div>
    



';
}



function listeProduit($db){
   echo '<h2>Liste des Produits </h2><br> ';
   
$produit = new produit($db);
$listeproduit = $produit->selectAll();

echo '<table class="table table-striped id="client" >';
        echo '<thead>';
            echo '<tr id="titre">';
                 echo ' <th>Photos</th>';
                echo ' <th>Identifiants</th>';
                echo ' <th>Designations</th>';
                echo ' <th>Prix</th>';
                echo ' <th>Id types</th>';
            echo '</tr>';
        echo '</thead>';
        echo '<TBODY>';
            foreach($listeproduit as $unProduit){
                echo '<tr>';
                    echo '<td> <img src="assets/img/slider/produits/'.$unProduit['photo'].'" alt="'.$unProduit['photo'].'" style="width:80px;height:80px;"></td>';
                    echo '<td>'.utf8_encode($unProduit['identifiant'] ).'</td>'; 
                    echo '<td>'.utf8_encode($unProduit['designation'] ).'</td>'; 
                    echo '<td>'.utf8_encode($unProduit['prix'] ).'</td>'; 
                    echo '<td>'.utf8_encode($unProduit['idtype'] ).'</td>';
                    
                   
            }
            echo '</tr>';
        echo '</TBODY>';
 echo '</table>';

 }
 
 
 
function ajoutProduit($db){
  
  
   
    if(isset($_FILES['photo'])){
        $extensions_ok = array('png', 'gif', 'jpg', 'jpeg');
        $taille_max = 500000;
        $dest_dossier = '/var/www/html/projet/assets/img/slider/produits/';
        if( !in_array( substr(strrchr($_FILES['photo']['name'], '.'), 1), $extensions_ok ) ){
            echo 'Veuillez sélectionner un fichier de type png, gif ou jpg !';
        }
        
        else{
            if( file_exists($_FILES['photo']['tmp_name'])&& (filesize($_FILES['photo']['tmp_name']))>$taille_max){
                echo 'Votre fichier doit faire moins de 500Ko !'; }
            else{
                // copie du fichier
                $dest_fichier = basename($_FILES['photo']['name']); // formatage nom fichier
                // enlever les accents
                $dest_fichier=strtr($dest_fichier,'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùú ûüýÿ','AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                // remplacer les caractères autres que lettres, chiffres et point par _
                $dest_fichier = preg_replace('/([^.a-z0-9]+)/i', '_', $dest_fichier);
                // copie du fichier
                move_uploaded_file($_FILES['photo']['tmp_name'],
                        $dest_dossier . $dest_fichier);
                
                $photo = $dest_fichier;
                
                $produit = new produit($db);
                
                $identifiant = $_POST['identifiant'];
                $designation = $_POST['designation'];
                $prix = $_POST['prix'];
                $idtype = $_POST['idtype'];
                
                $nb = $produit->insertAll($identifiant, $designation, $prix, $idtype, $photo);
                
                if ($nb!=1){
                echo '
                    <br>
                    <div class="alert alert-danger" role="alert">Attention erreur d\'insertion </div>
                    </div>
                    <a href="index.php?page=formproduitadmin">
                        <button class="btn btn-default">ESSAYER A NOUVEAU</button>
                    </a>
                    <br>
                    <br>
                    <br>
                    ';
                }

                else{
                echo'
                    <br>
                    <div class="alert alert-success" role="alert">Insertion réussie</div>
                    </div>
                    <a href="index.php?page=formproduitadmin">
                        <button class="btn btn-default">AJOUTER UN NOUVEAU PRODUIT</button>
                    </a>
                    <br>
                    <br>
                    <br>
                    ';
                } 
            }
        }
    }
    else {
                $photo = 'default.jpg';
                
                $produit = new produit($db);
                
                $identifiant = $_POST['identifiant'];
                $designation = $_POST['designation'];
                $prix = $_POST['prix'];
                $idtype = $_POST['idtype'];
                
                $nb = $produit->insertAll($identifiant, $designation, $prix, $idtype, $photo);
                
                if ($nb!=1){
                echo '
                    <br>
                    <div class="alert alert-danger" role="alert">Attention erreur d\'insertion default </div>
                    </div>
                    <a href="index.php?page=formproduitadmin">
                        <button class="btn btn-default">ESSAYER A NOUVEAU</button>
                    </a>
                    <br>
                    <br>
                    <br>
                    ';
                }

                else{
                echo'
                    <br>
                    <div class="alert alert-success" role="alert">Insertion réussie default</div>
                    </div>
                    <a href="index.php?page=formproduitadmin">
                        <button class="btn btn-default">AJOUTER UN NOUVEAU PRODUIT</button>
                    </a>
                    <br>
                    <br>
                    <br>
                    ';
                } 
    }

}

function supprimeProduit($db){
    
    if(isset($_POST['btSupprimer'])){
        
        if (isset($_POST['cocher'])){
            $liste = $_POST['cocher'];
            $produit = new produit($db);
            
            
            foreach($liste as $identifiant){
                $supp_fichier = $photo;
                $nb = $produit->deleteOne($identifiant);
                
            }
            
            
            Unlink($supp_fichier);
        }
    }

    echo '<h2>Suppression produit</h2><br>';
   
    $produit = new produit($db);
    $liste = $produit->selectAll();
    
    if (count($liste)>0){
        
        echo '<form method="post" action="index.php?page=supprimeproduitadmin">';
        echo '<table class="table table-striped">
                <THEAD>
                    <tr>
                        <th>Photos</th>
                        <th>Identifiants</th>
                        <th>Designations</th>
                        <th>Prix</th>
                        <th>Id types</th>
                        <th class="element">Sélectionner un ou des élèment(s) à supprimer</th>
                    </tr>
                <THEAD>
                <TBODY>
        ';

        foreach($liste as $unProduit){
            echo '<tr><td> <img src="assets/img/slider/produits/'.$unProduit['photo'].'" alt="'.$unProduit['photo'].'" style="width:80px;height:80px;"></td>';
            echo '<td>'.utf8_encode($unProduit['identifiant'] ).'</td>'; 
            echo '<td>'.utf8_encode($unProduit['designation'] ).'</td>'; 
            echo '<td>'.utf8_encode($unProduit['prix'] ).'</td>'; 
             echo '<td>'.utf8_encode($unProduit['idtype'] ).'</td>';
             
            echo '<td><input type="checkbox" name="cocher[]" value="'.$unProduit['identifiant'].'"/></td></tr>';
        }
        echo'</tr></TBODY>';
        echo '</table>';
        echo '<input type="submit" name="btSupprimer" value="Supprimer" />' ;
        echo '</form><br>';
    } 
    
    else{
    echo 'La liste est vide' ;
    
    }
}
function modifProduit($db){
    echo'MODIFICATION PRODUIT';
}
    
?>