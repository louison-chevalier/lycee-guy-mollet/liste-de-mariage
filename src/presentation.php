<?php

/******************************************************************************/
/******************* EN TÊTE QUI S'APPLIQUE SUR TOUS LE SITE ******************/
/******************************************************************************/

function entete(){
    
    echo'
    <!DOCTYPE html>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Site Mariage</title>


  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href="https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="assets/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="assets/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="assets/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="assets/css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="assets/css/style.css">

	<!-- Modernizr JS -->
	<script src="assets/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->

	</head>
	';
}



/******************************************************************************/
/********************* MENU QUI S'APPLIQUE SUR TOUS LE SITE *******************/
/******************************************************************************/

function menu(){
 
echo'<body>
        <div class="fh5co-loader"></div>
	
	<div id="page">
	<nav class="fh5co-nav" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-xs-2">
					<div id="fh5co-logo"><a href="">Debienne<strong>.</strong></a></div>
				</div>
				<div class="col-xs-10 text-right menu-1">
					<ul>
						<li class="active"><a href="index.php?page=accueil">Acceuil</a></li>
						<li class="active"><a href="index.php?page=cadeaux">Cadeaux souhaités</a></li>

						<li class="active"><a href="index.php?page=login">MON COMPTE</a></li>
						<li class="active"><a href="index.php?page=contact">Contact</a></li>';
						 
						 if($_SESSION){ 
							echo'<li class="active" style="color:red;"><a href="index.php?page=accueiladmin">[admin]</a></li>';
                         }

                         echo'
					</ul>
				</div>
			</div>
			
		</div>
	</nav>';
}
        
   
/******************************************************************************/
/***************** BAS DE PAGE QUI S'APPLIQUE SUR TOUS LE SITE ****************/
/******************************************************************************/

function bas(){
    
echo '

        </body>
    </html>';
}


?>
