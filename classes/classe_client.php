<?php class Client
{
    private $insertAll;
    
    private $selectAll;
    private $deleteOne;
    // Constructeur
    // Paramètre : Base de données
    
    
    public function __construct($db)
    {
    $this->insertAll = $db->prepare("INSERT INTO Client (email, mdp, nom, prenom, adresse, ville, cp, tel) VALUES (:email,:mdp, :nom, :prenom, :adresse, :ville, :cp, :tel)");
    $this->deleteOne = $db->prepare("delete from Client where email=:email ") ; 
    $this->selectAll = $db->prepare("SELECT * FROM Client Order by nom ASC");
    }
     
    
    public function insertAll($email, $mdp, $nom , $prenom, $adresse, $ville, $cp, $tel)
    {
    $this->insertAll->execute(array(''
        . ':email'=>$email,
          ':mdp'=>$mdp,
          ':nom' => $nom,
          ':prenom' => $prenom,
          ':adresse' => $adresse,
          ':ville' => $ville,
          ':cp' => $cp,
          ':tel' => $tel));
    return $this->insertAll->rowCount();}
        
    
    public function deleteOne($email){
    $this->deleteOne->execute(array(':email'=>$email));
    return $this->deleteOne->rowCount();
    }


    public function selectAll(){
        $this->selectAll->execute();
        return $this->selectAll->fetchAll();
        
    }
    
    
} ?>