<?php class Produit
{
    private $insertAll;
    private $deleteOne;
    private $selectAll;
    private $selectOne;
    // Constructeur
    // Paramètre : Base de données
    
    
    public function __construct($db)
    {
    $this->insertAll = $db->prepare("INSERT INTO Produit (identifiant, designation, prix, idtype, photo) VALUES (:identifiant,:designation, :prix, :idtype, :photo)");
    $this->selectAll = $db->prepare("SELECT * FROM Produit");
    $this->selectOne = $db->prepare("select * FROM Produit where idtype=:idtype");
    $this->deleteOne = $db->prepare("delete from Produit where identifiant=:identifiant") ; 
    }
     
    
    public function insertAll($identifiant, $designation, $prix , $idtype, $photo){
    $this->insertAll->execute(array(':identifiant'=>$identifiant,':designation'=>$designation,
        ':prix' => $prix, ':idtype' => $idtype,':photo' => $photo
));
    return $this->insertAll->rowCount();}
        
    public function selectAll(){
    $this->selectAll->execute();
    return $this->selectAll->fetchAll();
    }
    
    public function deleteOne($identifiant){
    $this->deleteOne->execute(array(':identifiant'=>$identifiant));
    return $this->deleteOne->rowCount();
}
    public function selectOne($idtype){ 
    $this->selectOne->execute(array(':idtype'=>$idtype)); 
    return $this->selectOne->fetchAll();
}
    
} ?>