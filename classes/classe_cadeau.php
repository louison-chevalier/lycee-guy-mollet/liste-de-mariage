<?php class Cadeau
{
    private $insertAll;
    private $deleteOne;
    private $selectAll;
    private $selectOne;
    // Constructeur
    // Paramètre : Base de données
    
    
    public function __construct($db)
    {
    $this->insertAll = $db->prepare("INSERT INTO cadeau (numero, nom, prix, etat, idtype, photo, liens) VALUES (:numero, :nom, :prix, :etat, :idtype, :photo, :liens)");
    $this->selectAll = $db->prepare("SELECT * FROM cadeau");
    $this->selectOne = $db->prepare("select * FROM cadeau where idtype=:idtype");
    $this->deleteOne = $db->prepare("delete from cadeau where numero=:numero") ; 
    }
     
    
    public function insertAll($numero, $nom, $prix , $etat, $idtype, $photo, $liens){
    $this->insertAll->execute(array(':numero'=>$numero,':nom'=>$nom,
        ':prix' => $prix, ':etat' => $etat,':idtype' => $idtype,':photo' => $photo, ':liens' => $liens
            ));
    return $this->insertAll->rowCount();}
        
    public function selectAll(){
    $this->selectAll->execute();
    return $this->selectAll->fetchAll();
    }
    
    public function deleteOne($numero){
    $this->deleteOne->execute(array(':numero'=>$numero));
    return $this->deleteOne->rowCount();
}
    public function selectOne($idtype){ 
    $this->selectOne->execute(array(':idtype'=>$idtype)); 
    return $this->selectOne->fetchAll();
}
    
} ?>