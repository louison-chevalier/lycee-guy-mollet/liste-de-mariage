# Mariage

SIO2D Challenge de la rentrée – Mariage septembre 2017 - Lycée Guy Mollet Arras

**Thème imposé : Liste de mariage**

**Réalisation d'un prototype répondant aux besoins suivants :**   
M. Debienne va se marier prochainement. Il a élaboré une liste de cadeaux qu'il veut soumettre à ses invités de manière sécurisée.   
Ainsi chaque invité qui le désire pourra choisir dans cette liste un ou plusieurs cadeaux.    
La liste sera alors mise à jour pour éviter les redondances dans les achats.   
Dès qu'un choix est effectué, M. Debienne recevra un email, l'informant du choix de l'invité. M. Debienne est apprenti développeur et souhaite donc avoir un cours ou dossier technique lui expliquant clairement comment réaliser le code de ces différentes tâches.   
  
**Définition d’un prototype (Wikipedia) :**   
Il s'agit aussi d'un exemplaire incomplet (et non définitif) de ce que pourra être un produit (éventuellement de type logiciel ou de type « service »).

**Début du challenge :** 
Le 05/09/2017 à 10h10

**Fin du challenge :**
Présentation des prototypes : le 12/09/2017 à 10h00

**Remise du dossier technique :**
Le 12/09/2017 à 15h00 ou à 17h00 Attention, les séances de cours ne seront pas consacrées à la réalisation de ce challenge.   
Matières concernées : tous les modules de 1ère année

**Organisation :**   
Travail en équipe de 2 personnes. Chaque personne doit disposer d’un ordinateur pour travailler. Des points seront retirés si ce n’est pas le cas.

**Langages à utiliser :**   
PHP, HTML, CSS

**Système à utiliser :**   
Debian 8 + MySQL

**Travail à faire :**  
- Répondre aux besoins décrits ci-dessus.  
- Rédiger un dossier technique illustré de copies d'écran, qui décrira le code de chaque page et de chaque fonctionnalité. Un développeur devra être capable de comprendre facilement votre code.

**Évaluation du travail :**  
- Votre travail sera évalué sur la qualité du prototype rendu, mais aussi sur la présentation que vous en ferez. La note obtenue comptera en SLAM3 et en SLAM5 et il y aura qu’une note par équipe.   
- Le prototype devra être fonctionnel, il faudra donc désactiver les fonctions qui ne sont pas au point.   
- Vous devez exploiter tout le temps imparti pour proposer une solution originale. Vous êtes libre de vous aider entre groupes tant que vos projets restent inédits et uniques.


**Comment rendre le travail ?**   
Vous aurez, le 12/09/17 dès 08h00, accès à une machine sur laquelle vous installerez votre projet. Vous rendrez également le schéma (MCD) de votre BdD sur papier exclusivement ainsi que toutes les requêtes réalisées dans le projet avec un titre explicatif pour chacune d'elles.
Mais aussi : une présentation de votre projet sous la forme d’un diaporama que vous présenterez devant les personnes de votre groupe (durée : entre 5 et 10mn).
Suivant votre groupe, à 15h00 ou à 17h00 le 12/09/2017, vous nous remettrez au format PDF votre dossier technique.
Vous gérez votre temps à votre guise. Vous avez la possibilité de sortir 5 minutes à chaque sonnerie.


